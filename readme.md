# Sync scoutbook with mailchimp
1. setup config.cfg
2. login to scoutbook on chrome
3. run sb.py to get a roster.json
4. run chimp.py to add missing users

## config.cfg
> CHIMP_API = "your_api_key"
> UNIT_ID = your_scoutbook_unit_id
> COOKIE_FILE = "/Users/username/Library/Application Support/Google/Chrome/Profile 1/Cookies"
> MAILCHIMP_LIST_NAME = "Newsletter"

## sb.py
requires: Cookie from chrome (login to scoutbook in chrome)
This script will scrape the website to get a json version of the users/scouts and their parents.

## chimp.py
This script will take the roster.json and compare the email addresses to the ones that are in the mailchimp, adding where needed.