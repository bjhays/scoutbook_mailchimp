"""Compare and add emails to mailchimp from roster."""
from mailchimp3 import MailChimp
from os import path, getenv
from dotenv import load_dotenv
import pprint
import json

load_dotenv("config.cfg")
pp = pprint.PrettyPrinter(indent=4)
API_KEY = getenv("CHIMP_API")
LIST_NAME = getenv("MAILCHIMP_LIST_NAME")
client = MailChimp(mc_api=API_KEY)

if not path.exists("roster.json"):
    raise Exception("Roster not found, run run2.py")

with open("roster.json") as f:
    roster = json.load(f)

emails = {}
for sid in roster:
    for p in roster[sid]['parents']:
        emails[str(p['email']).lower()] = p

print("Found %s emails from roster.json" % len(emails))

# returns all the lists (only name and id)
# {'lists': [{'id': 'e1bbafd72f', 'name': 'Newsletter'}], 'total_items': 1}
cl = client.lists.all(get_all=True, fields="lists.name,lists.id")
for l in cl['lists']:
    if l['name'] == 'Newsletter':
        list_id = l['id']

if list_id is None:
    raise Exception("Newsletter List not found")

chimp_emails = []
# returns all members inside list
# get_all=True,   fields="members.email_address,members.status,members.id",  count=1, offset=0
for m in client.lists.members.all(list_id, fields="members.email_address", get_all=True)['members']:
    chimp_emails.append(m['email_address'].lower())

cset = set(chimp_emails)
eset = set(emails.keys())

print("found %s from mailchimp" % len(chimp_emails))
pp.pprint(chimp_emails)

print("Missing the following emails in mailchimp:")
for e in list(eset - cset):
    print(emails[e])
    fname, lname = emails[e]['name'].split(" ", 1)
    try:
        client.lists.members.create(list_id, {
            'email_address': e,
            'status': 'subscribed',
            'merge_fields': {
                'FNAME': fname,
                'LNAME': lname,
            },
        })
    except Exception as e:
        print("Error: %s" % e)


print("In chimp not in scoutbook emails:")
pp.pprint(list(cset - eset))
