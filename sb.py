"""Testing 123."""
from os import path, makedirs, getenv
from pycookiecheat import chrome_cookies
import requests
from bs4 import BeautifulSoup
import re
import pprint
import logging
import json
from dotenv import load_dotenv

load_dotenv("config.cfg")

logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger('main')

pp = pprint.PrettyPrinter(indent=4)
UNIT_ID = getenv("UNIT_ID")
base_url = "https://www.scoutbook.com"
star_url = "%s/mobile/dashboard/admin/roster.asp?UnitID=%s" % (base_url, UNIT_ID)

cookies = chrome_cookies(base_url,
                         cookie_file=getenv("COOKIE_FILE"))

if not path.exists("cache"):
    makedirs("cache")


def get_raw_url(url, cache_name):
    """Get raw html from url if needed."""
    cache_file = "cache/%s.html" % cache_name
    logger.debug("reading %s" % cache_file)
    if not path.exists(cache_file):
        r = requests.get(url, cookies=cookies)
        content = str(r.text)
        with open(cache_file, "w") as f:
            f.write(content)
    else:
        with open(cache_file) as f:
            content = f.read()
    return content


def get_info(url):
    """Parse scout's parent info from url.

    returns dict with scout_id and a list of parent's links
    """
    regex = r"ScoutUserID=([0-9]+)"
    res = re.findall(regex, url)[0]
    content = get_raw_url(url, res)
    soup = BeautifulSoup(content, 'html.parser')
    elems = soup.find_all('a', href=re.compile("ConnectionID"), text=True)
    uniq = {}
    # pp.pprint(elems)
    for e in elems:
        # print(e.__dict__)
        uniq[e['href']] = e.contents
    # pp.pprint(uniq)
    return {"scout_id": res, "parent_links": uniq}


def get_parent_contact_info(name, url):
    """Parse parent contact information."""
    regex = r"ScoutUserID=([0-9]+)&ConnectionID=([0-9]+)&UnitID=([0-9]+)&DenID=([0-9]+)&PatrolID=([0-9]+)?"
    res = "_".join(re.findall(regex, url)[0]).strip('_')
    content = get_raw_url(url, res)
    # print(content)
    soup = BeautifulSoup(content, 'html.parser')
    try:
        email = soup.find_all('a', href=re.compile("MAILTO"))
        if len(email) > 0:
            email = email[0]['href'].replace("MAILTO:", "")
    except Exception as e:
        logger.debug("%s: %s" % (res, e))
        email = None

    try:
        tel = soup.find_all('a', href=re.compile("tel:"))
    except:
        tel = None
    # unit = soup.find_all('#goToUnit') # [0].contents[0]
    # den = soup.find_all('#goToDenPatrol') # [0].contents[0]
    # scout = soup.find_all('#account') # [0].contents[0]
    if len(tel) > 0:
        first_num = tel[0].contents[0]
    else:
        first_num = None
    # pp.pprint(tel)
    return {"name": name[0],
            "email": email,
            "tel": first_num,
            "id": res,
            # "unit": unit,
            # "den": den,
            # "scout": scout,
            }


def link_is_scout(href):
    """Helper for soup to determine if link is a scout."""
    if href is None:
        return False
    if "ScoutUserID" in href and "ConnectionID" not in href:
        return True
    else:
        return False


def get_roster(star_url):
    """Get the main roster page, parse and return roster."""
    roster = {}
    if not path.exists("cache.txt"):
        logger.debug("get new")
        r = requests.get(star_url, cookies=cookies)
        # print(r.__dict__)
        logger.debug("url from request %s" % r.url)
        content = str(r.text)
        with open("cache.txt", "w") as f:
            f.write(content)

    else:
        with open("cache.txt") as f:
            content = f.read()

    if "Dooley" in content:
        logger.debug("found Dooley ")
    else:
        logger.debug("Dooley not found")

    soup = BeautifulSoup(content, 'html.parser')
    elems = soup.find_all('a', href=link_is_scout)  # href=re.compile("ScoutUserID"))
    for e in elems:
        # print(e)
        scout = str(e.contents[0]).strip()
        logger.debug("%s: %s" % (scout, str(e).strip()))
        sinfo = get_info("%s%s" % (base_url, e['href']))
        sid = sinfo['scout_id']
        roster[sid] = {"scout": scout, "sid": sid, "parents": []}
        if not sinfo['parent_links']:
            raise Exception('Boom')
        for url in sinfo['parent_links']:
            name = sinfo['parent_links'][url]
            info = get_parent_contact_info(name, base_url + url)
            roster[sid]['parents'].append(info)
    return roster

if __name__ == '__main__':
    logger.debug(star_url)
    print("writing to 'roster.json'")
    with open("roster.json", "w") as f:
        f.write(json.dumps(get_roster(star_url)))
